"use client";

import { useDataSchool } from "@/hooks/shared.hooks";
import moment from "moment";
import Link from "next/link";
import Image from "next/image";
import { Box, Button, Text } from "@chakra-ui/react";
import React, { useState } from "react";
import LoginWithPhoneNumber from "@/components/login/login-with-phone";
import LoginWithEmail from "@/components/login/login-with-email";
import { useParams, useRouter } from "next/navigation";

export default function Home() {
  const subdomain = window.location.hostname.split(".")[0];
  const { data, errorMessage } = useDataSchool({ page: 1 });
  const { by } = useParams();
  const [isUsingEmail, setIsUsingEmail] = useState(true);
  const thisYear = moment().format("YYYY");
  const nextYear = moment().add(1, "year").format("YYYY");
  const periode = `${thisYear}/${nextYear}`;
  const router = useRouter();
  // console.log({ data: data?.data?.find((res) => res.domain == subdomain) });

  const page = {
    with_email: <LoginWithEmail setIsUsingEmail={setIsUsingEmail} />,
    with_phone: <LoginWithPhoneNumber setIsUsingEmail={setIsUsingEmail} />,
  };
  return (
    <Box minHeight="100vh" py="32px" width="100vw" display={{ base: "flex" }} flexDir={{ base: "column", lg: "row" }} justifyContent="center" alignItems="center" gap="42px" px={{ base: "16px", md: "32px" }} bg="#90CDF4">
      <Box width={{ base: "100%", xl: "60vw" }} height="100%" display={{ base: "flex", xl: "block" }} flexDirection="column" justifyContent={{ base: "center", xl: "flex-start" }} alignItems={{ base: "center", xl: "flex-start" }}>
        <Box textAlign="center" fontSize={{ base: "16px", md: "24px" }} fontWeight={600}>
          <Text fontSize={{ base: "24px", xl: "32px" }} fontWeight={700} textAlign="center">
            SELAMAT DATANG PESERTA DIDIK BARU {periode}
          </Text>
          <Text
            maxW={"567px"}
            fontWeight={"400"}
            // border={"1px solid black"}
            mx={"auto"}
          >
            Sebelum mengisi formulir pendaftaran, silahkan register atau login terlebih dahulu
          </Text>
        </Box>
        <Box display="flex" flexDir="column" alignItems="center" justifyContent="center" gap="16px" pt="24px">
          <Button bgColor="#2C5282" color="white" fontWeight={"400"} _hover="none" onClick={() => router.push("/register")}>
            Buat Akun Baru
          </Button>
        </Box>
        <Box mt="49px" mx="auto" display={{ base: "none", lg: "block" }}>
          <Image src="/hello.svg" alt="hello" width={600} height={400} />
        </Box>
      </Box>
      <Box minW="40vw" height={{ base: "65vh", xl: "85vh" }} boxShadow={"0px 4px 8px rgba(0, 0, 0, 0.25)"} rounded={8}>
        {isUsingEmail ? page.with_phone : page.with_email}
      </Box>
    </Box>
  );
}
