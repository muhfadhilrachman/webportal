"use client";

import { Box, Button, Text } from "@chakra-ui/react";
import React, { useState } from "react";
import LoginWithPhoneNumber from "../../../components/login/login-with-phone";
import LoginWithEmail from "../../../components/login/login-with-email";
import { useParams, useRouter } from "next/navigation";
import moment from "moment";
import Link from "next/link";
import Image from "next/image";

const Login = () => {
  const { by } = useParams();
  const [isUsingEmail, setIsUsingEmail] = useState(true);
  const thisYear = moment().format("YYYY");
  const nextYear = moment().add(1, "year").format("YYYY");
  const periode = `${thisYear}/${nextYear}`;
  const router = useRouter();
  // console.log({ data: data?.data?.find((res) => res.domain == subdomain) });

  const page = {
    with_email: <LoginWithEmail setIsUsingEmail={setIsUsingEmail} />,
    with_phone: <LoginWithPhoneNumber setIsUsingEmail={setIsUsingEmail} />,
  };
  return (
    <Box minHeight="100vh" width="100vw" display={{ base: "flex" }} justifyContent="center" alignItems="center" gap="32px" px={{ base: "16px", md: "32px" }} bg="#90CDF4">
      <Box minW="40vw" height="80vh" boxShadow={"0px 4px 8px rgba(0, 0, 0, 0.25)"} rounded={8} display={{ base: "none", lg: "block" }}>
        {page[by]}
      </Box>
    </Box>
  );
};

export default Login;
