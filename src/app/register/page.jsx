"use client";

import HeaderLoginPpdb from "@/components/PPDB/header";
import FormRegister from "@/components/register/formRegister";
import { Box, Button, Text } from "@chakra-ui/react";
import moment from "moment";
import Image from "next/image";
import { useRouter } from "next/navigation";
import { useState } from "react";

export default function Register() {
  const [isUsingEmail, setIsUsingEmail] = useState(true);
  const thisYear = moment().format("YYYY");
  const nextYear = moment().add(1, "year").format("YYYY");
  const periode = `${thisYear}/${nextYear}`;
  const router = useRouter();
  return (
    <Box minHeight="100vh" py="32px" width="100vw" display={{ base: "flex" }} flexDir={{ base: "column", lg: "row" }} justifyContent="center" alignItems="center" gap="42px" px={{ base: "16px", md: "32px" }} bg="#90CDF4">
      <Box width={{ base: "100%", xl: "60vw" }} height="100%" display={{ base: "flex", xl: "block" }} flexDirection="column" justifyContent={{ base: "center", xl: "flex-start" }} alignItems={{ base: "center", xl: "flex-start" }}>
        <Box textAlign="center" fontSize={{ base: "16px", md: "24px" }} fontWeight={600}>
          <Text fontSize={{ base: "24px", xl: "32px" }} fontWeight={700} textAlign="center">
            SELAMAT DATANG PESERTA DIDIK BARU {periode}
          </Text>
          <Text
            maxW={"567px"}
            fontWeight={"400"}
            // border={"1px solid black"}
            mx={"auto"}
          >
            Sebelum mengisi formulir pendaftaran, silahkan register atau login terlebih dahulu
          </Text>
        </Box>
        <Box display="flex" flexDir="column" alignItems="center" justifyContent="center" gap="16px" pt="24px">
          <Button bgColor="#2C5282" color="white" fontWeight={"400"} _hover="none" onClick={() => router.push("/")}>
            Sudah Punya Akun
          </Button>
        </Box>
        <Box mt="49px" mx="auto" display={{ base: "none", lg: "flex" }} justifyContent="center" alignItems="center">
          <Image src="/siginUp.svg" alt="hello" width={500} height={250} />
        </Box>
      </Box>
      <Box minW={{ base: "80vw", lg: "45vw" }} boxShadow={"0px 4px 8px rgba(0, 0, 0, 0.25)"} rounded={8} bg=" #BEE3F8;">
        <FormRegister setIsUsingEmail={setIsUsingEmail} isUsingEmail={isUsingEmail} />
      </Box>
    </Box>
  );
}
